<?php
include 'includes/header.php';

$dia = date("d", strtotime($programa['fechaE']));
$mes = date("m", strtotime($programa['fechaE']));
$año = date("Y", strtotime($programa['fechaE']));

$especialistaSelect = explode(',', $programa['especialista']);
$tecnicaSelect = explode(',', $programa['tecnica']);


switch ($mes) {
    case "01":
        $mesS = "Enero";
        break;
    case "02":
        $mesS = "Febrero";
        break;
    case "03":
        $mesS = "Marzo";
        break;
    case "04":
        $mesS = "Abril";
        break;
    case "05":
        $mesS = "Mayo";
        break;
    case "06":
        $mesS = "Junio";
        break;
    case "07":
        $mesS = "Julio";
        break;
    case "08":
        $mesS = "Agosto";
        break;
    case "09":
        $mesS = "Septiembre";
        break;
    case "10":
        $mesS = "Octubre";
        break;
    case "11":
        $mesS = "Noviembre";
        break;
    case "12":
        $mesS = "Diciembre";
        break;
    default:
        break;
}
$fechaEstreno = $dia . " de " . $mesS . " de " . $año;
?>


<!doctype html>
<html>
    <head>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151480327-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-151480327-1');
        </script>


        <meta name="title" content="ExpoHobby TV">
        <meta name="description" content="Todo el contenido de ExpoHobby.">
        <meta name="keywords" content="expohobby,hobby,tv,programas,episodios,arte,manualidades,arte realeggza,costura & patchwork,chocolatería,fussingglue,decoracion de tortas,mascaras venecianas,modelado,goma eva,muñequería,pascua,navidad,pintura,decorativa,porcelana fría,quilling,repujado de estaño,scrap,sublimación,vitrofusion">
        <meta name="robots" content="index, follow">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="Spanish">
        <meta name="revisit-after" content="30 days">
        <meta name="author" content="ExpoHobby">


        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />

        <link rel="stylesheet" href="css/font-icons.css" type="text/css" />

        <link href="css/ehtv-programa.css" rel="stylesheet" type="text/css">


        <!-- Owl Stylesheets -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.default.css">

        <link href="css/animate.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">

        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- javascript -->

        <script type="text/javascript" src="js/jquery-3.4.1.js"></script>
        <script type="text/javascript" src="js/owl.carousel.js"></script>

        <script src="js/modernizr.custom.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/boxesFx.js"></script>

        <script src="js/jquery.fittext.js"></script>
        <script src="js/jquery.lettering.js"></script>
        <script src="js/jquery.textillate.js"></script>

        <script src="js/jquery.fancybox.js"></script> 

        <script type="text/javascript">

            $(document).ready(function () {
                $('#btn-que-top').click(function () {
                    $('#panel-que-top').css("transform", "translate(0,500px)");
                    $('#panel-tele-top').css("transform", "translate(0,0px)");
                    $('#panel-site').css("transform", "translate(0,500px)");
                });
                $('#btn-que-top-x').click(function () {
                    $('#panel-que-top').css("transform", "translate(0,0px)");
                    $('#panel-site').css("transform", "translate(0,0px)");
                });
                $('#btn-tele-top').click(function () {
                    $('#panel-tele-top').css("transform", "translate(0,500px)");
                    $('#panel-que-top').css("transform", "translate(0,0px)");
                    $('#panel-site').css("transform", "translate(0,500px)");
                });
                $('#btn-tele-top-x').click(function () {
                    $('#panel-tele-top').css("transform", "translate(0,0px)");
                    $('#panel-site').css("transform", "translate(0,0px)");
                });



                var owl = $('#prof');
                owl.owlCarousel({
                    margin: 30,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 5
                        }
                    }
                })

                var owl2 = $('#tecnicas');
                owl2.owlCarousel({
                    margin: 30,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 2000,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 5
                        }
                    }
                })

            });


        </script>        


        <title>ExpoHobby TV</title>


    </head>

    <body>

        <div class="panel-que-ver" id="panel-que-top">
            <div class="top">
                <div class="wrapper">
                    <div class="titulo">¿Qué querés ver?</div>
                    <div class="cerrar" id="btn-que-top-x"></div>
                </div>
            </div>
            <div class="bottom">
                <div class="contenido">
                    <div class="wrapper">
                        <div class="col">
                            <h5>Temporada</h5>
                            <select id="temp" class="temp" onchange="cambioTemp(1)"> 
                                <option value="0">Todas</option>
                                <?php
                                foreach ($temporada as $temp) {
                                    if ($temp_id == $temp['cod']) {
                                        ?>
                                        <option selected value="<?php echo $temp['cod']; ?>" ><?php echo $temp['nombre']; ?></option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="<?php echo $temp['cod']; ?>" ><?php echo $temp['nombre']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <h5>Tipo</h5>
                            <select id="tipo" class="tipo" onchange="">
                                 <option value="0" <?php if ($_GET['tipo']==0){echo "selected";} ?>>Todos</option>
                                <option value="1" <?php if ($_GET['tipo']==1){echo "selected";} ?>>Programas</option>
                                <option value="2" <?php if ($_GET['tipo']==2){echo "selected";} ?>>Masters</option>
                                <option value="3" <?php if ($_GET['tipo']==3){echo "selected";} ?>>Vivos</option>
                               <!-- <option value="4">Especiales</option> -->
                            </select>
                        </div>
                        
                        <div class="col">
                            <h5>Técnica</h5>
                            <select id="tec" class="tec" onchange="cambioFiltroTecnica(1)">
                                <option value="0">Todas</option>
                                <?php
                                foreach ($tecnica as $tec) {
                                    if ($tec_id == $tec['cod']) {
                                        ?>
                                        <option selected value="<?php echo $tec['cod']; ?>"><?php echo $tec['nombre']; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $tec['cod']; ?>"><?php echo $tec['nombre']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <h5>Especialista</h5>
                            <select id="esp" class="esp">
                                <option value="0">Todos</option>
                                <?php
                                foreach ($especialista as $temp) {
                                    if ($esp_id == $temp['cod']) {
                                        ?>
                                        <option selected value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <h5>Auspiciante</h5>
                            <select id="ausp" class="ausp">
                              <option value="0">Todos</option>
                                    <?php
                                    foreach ($auspiciantes as $ausp) {
                                        if ($ausp == $ausp['cod']) {
                                            ?>
                                            <option selected value="<?php echo $ausp['cod']; ?>"><?php echo $ausp['nombre']; ?></option>
                                        <?php } else {
                                            ?>
                                            <option value="<?php echo $ausp['cod']; ?>"><?php echo $ausp['nombre']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                            </select>
                        </div>
                        
                        <div class="col filtrar">
                            <button>Filtrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-tele" id="panel-tele-top">
            <div class="top">
                <div class="wrapper">
                    <div class="titulo">Miranos como en la Tele!</div>
                    <div class="cerrar" id="btn-tele-top-x"></div>
                </div>
            </div>
            <div class="bottom">
                <div class="contenido">
                    <div class="wrapper">
                        <div class="col">
                            <h5>Temporada</h5>
                            <select id="temp-m" class="temp-m" onchange="cambioTemp(2)">
                                <option value="0">Todas</option>
                                <?php
                                foreach ($temporada as $temp) {
                                    if ($temp_id == $temp['cod']) {
                                        ?>
                                        <option selected value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                        <?php
                                    }
                                }
                                ?>																				
                            </select>
                        </div>
                        <div class="col">
                            <h5>Tipo</h5>
                            <select id="tipo-m" class="tipo-m" onchange="">
                                 <option value="0" <?php if ($_GET['tipo']==0){echo "selected";} ?>>Todos</option>
                                <option value="1" <?php if ($_GET['tipo']==1){echo "selected";} ?>>Programas</option>
                                <option value="2" <?php if ($_GET['tipo']==2){echo "selected";} ?>>Masters</option>
                                <option value="3" <?php if ($_GET['tipo']==3){echo "selected";} ?>>Vivos</option>
                               <!-- <option value="4">Especiales</option> -->
                            </select>
                        </div>
                        
                        <div class="col">
                            <h5>Técnica</h5>
                            <select id="tec-m" class="tec-m" onchange="cambioFiltroTecnica(2)">
                                <option value="0">Todas</option>
                                <?php
                                foreach ($tecnica as $temp) {
                                    if ($tec_id == $temp['cod']) {
                                        ?>
                                        <option selected value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <h5>Especialista</h5>
                            <select id="esp-m" class="esp-m">
                                <option value="0">Todos</option>
                                <?php
                                foreach ($especialista as $temp) {
                                    if ($esp_id == $temp['cod']) {
                                        ?>
                                        <option selected value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $temp['cod']; ?>"><?php echo $temp['nombre']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <h5>Auspiciante</h5>
                            <select id="ausp-m" class="ausp-m">
                                <option value="0">Todos</option>
                                    <?php
                                    foreach ($auspiciantes as $ausp) {
                                        if ($ausp == $ausp['cod']) {
                                            ?>
                                            <option selected value="<?php echo $ausp['cod']; ?>"><?php echo $ausp['nombre']; ?></option>
                                        <?php } else {
                                            ?>
                                            <option value="<?php echo $ausp['cod']; ?>"><?php echo $ausp['nombre']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                            </select>
                        </div>
                        
                        <div class="col busqueda_home">
                            <button>Iniciar Reproducción</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="site" id="panel-site">

            <div class="all">
                <div class="top-wt">
                    <div class="top" style="background-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">
                        <div class="menu">
                            <div class="home"><a href="index" title="Ir a la Home"></a></div>	                        
                            <div class="que-ver" id="btn-que-top"><h4>¿Qué querés ver?</h4></div>
                            <div class="tele" id="btn-tele-top"><h4>Miranos como en la Tele</h4></div>
                            <div onclick="location.href = 'resultado.php?temporada=0&tecnica=0&especialista=0';" class="ver-todo"><h4><a href="resultado.php?temporada=0&tecnica=0&especialista=0">Ver Todo</a></h4></div>
                        </div>
                        <div class="logo">
                            <a href="index">
                                <img src="images/ehtv-logo-bn.png" width="" height="" />
                            </a>
                        </div>
                    </div>

                    <div class="titulo" style="background-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">
                        <div class="wrapper">
                            <h4><?php echo $programa['titulo']; ?></h4>
                        </div>
                    </div>                    
                    <div class="color-sp" style="background-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,0.8);"></div>
                    <div class="filtros">
                        <div class="wrapper">
                            <div class="col">
                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Temporada</h5>
                                <h4><?php echo $programa['temporada']; ?></h4>
                            </div>
                            <div class="col">
                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Episodio</h5>
                                <h4><?php echo $programa['episodio']; ?></h4>
                            </div>
                            <div class="col">
                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Técnica</h5>
                                <h4>
                                    <?php
                                    $i = 0;
                                    foreach ($tecnica as $tec) {

                                        if (in_array($tec['cod'], $tecnicaSelect)) {
                                            //var_dump(count($especialistaSelect));
                                            if (count($tecnicaSelect) == 1) {
                                                ?>
                                                <?php echo $tec['nombre']; ?>
                                                <?php
                                            } else {
                                                if ($i == 0) {
                                                    ?>
                                                    <?php echo $tec['nombre']; ?>
                                                    <?php
                                                } else {
                                                    ?>
                                                    / <?php echo $tec['nombre']; ?>
                                                    <?php
                                                }
                                                $i++;
                                            }
                                        }
                                    }
                                    ?>
                                </h4>
                            </div>
                            <div class="col">
                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Especialistas</h5>
                                <h4>
                                    <?php
                                    $i = 0;
                                    foreach ($especialista as $esp) {
                                        if (in_array($esp['cod'], $especialistaSelect)) {
                                            //var_dump(count($especialistaSelect));
                                            if (count($especialistaSelect) == 1) {
                                                ?>
                                                <?php echo $esp['nombre']; ?>
                                                <?php
                                            } else {
                                                if ($i == 0) {
                                                    ?>
                                                    <?php echo $esp['nombre']; ?>
                                                    <?php
                                                } else {
                                                    ?>
                                                    / <?php echo $esp['nombre']; ?>                                                    <?php
                                                }
                                                $i++;
                                            }
                                        }
                                    }
                                    ?>
                                </h4>

                            </div>
                            <div class="col">
                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Fecha de Estreno</h5>
                                <h4><?php echo $fechaEstreno; ?></h4>
                            </div>
                            <div class="col share">
                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Compartí este Episodio</h5>
                                <div class="social">
                                    <a href="http://www.facebook.com/sharer.php?u=https://expohobby.tv/ver-programa?p=<?php echo $programa['cod']; ?>" target="_blank"><div class="fb"></div></a>
                                    <a href="https://twitter.com/share?url=https://expohobby.tv/ver-programa?p=<?php echo $programa['cod']; ?>&text=Mirando en Expo hobby tv &hashtags=#expohobbytv" target="_blank"><div class="tw"></div></a>
                                    <a href="#"><div style="display: none;" class="ig"></div></a>	                                	                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="container clearfix">

                    <div class="contenido">
                        <div class="bloque">
                            <div class="wrapper">  
                                <div class="video">
                                    <div id="player"></div>									
                                </div>

                                <div class="info">
                                    <div class="info-l">

                                        <div class="desc">
                                            <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Resumen</h5>
                                            <p>
                                                <?php echo $programa['desc']; ?>
                                            </p>
                                        </div>                                        




                                        <div class="imagenes">
                                            <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1); margin-top: 40px;">Imágenes del Programa</h5>
                                            <ul>
                                                <?php foreach ($imgPro as $foto){?>
                                                    <li>
                                                        <img src="recursos/galeria/<?php echo $foto['foto']; ?>"  alt="" width="" height="" />
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>	                            

                                    </div>
                                    <div class="info-r">
                                        <?php if (!empty($programa['t1'])) { ?>
                                            <div class="materiales">
                                                <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Materiales</h5>
                                                <div class="pdf">
                                                    <a href="recursos/materiales/<?php echo $programa['a1']; ?>"  style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);" target="_blank"><?php echo $programa['t1']; ?></a>
                                                </div>
                                                <?php if (!empty($programa['t2'])) { ?>
                                                    <div class="pdf">
                                                        <a href="recursos/materiales/<?php echo $programa['a2']; ?>"  style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);" target="_blank"><?php echo $programa['t2']; ?></a>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($programa['t3'])) { ?>
                                                    <div class="pdf">
                                                        <a href="recursos/materiales/<?php echo $programa['a3']; ?>"  style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);" target="_blank"><?php echo $programa['t3']; ?></a>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($programa['t4'])) { ?>
                                                    <div class="pdf">
                                                        <a href="recursos/materiales/<?php echo $programa['a4']; ?>"  style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);" target="_blank"><?php echo $programa['t4']; ?></a>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($programa['t5'])) { ?>
                                                    <div class="pdf">
                                                        <a href="recursos/materiales/<?php echo $programa['a5']; ?>"  style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);" target="_blank"><?php echo $programa['t5']; ?></a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>


                                        <div class="bio">
                                            <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Especialistas</h5>
                                            <ul>
                                                <?php
                                                foreach ($especialista as $value) {
                                                    $espSelect = explode(',', $programa['especialista']);
                                                    if (in_array($value['cod'], $espSelect)) {
                                                        ?>
                                                        <li>

                                                            <div class="avatar">
                                                                <?php if (!empty($value['foto'])) { ?>
                                                                    <img src="recursos/especialista/<?php echo $value['foto']; ?>" alt="none" width="" height="" />
                                                                <?php } else { ?>
                                                                    <img src="recursos/especialista/logo-none.jpg" alt="none" width="" height="" />
                                                                <?php } ?>
                                                            </div>                     
                                                            <h4 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);"><?php echo $value['nombre']; ?></h4>
                                                            <h5>
                                                                <a data-fancybox data-src="#info-<?php echo $value['cod']; ?>" href="javascript:;" style="background-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">+</a>
                                                            </h5>

                                                            <div id="info-<?php echo $value['cod']; ?>" class="especialista-info" style="display: none; border-top-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">
                                                                <div class="col-i">
                                                                    <h2 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);"><?php echo $value['nombre']; ?></h2>
                                                                    <div class="sep"></div>
                                                                    <?php if (!empty($value['email'])) { ?>
                                                                        <h6 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Mail: <a href="mailto:<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a></h6>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="col-d">
                                                                    <div class="avatar">
                                                                        <?php if (!empty($value['foto'])) { ?>
                                                                            <img src="recursos/especialista/<?php echo $value['foto']; ?>" alt="none" width="" height="" />
                                                                        <?php } else { ?>
                                                                            <img src="recursos/especialista/logo-none.jpg" alt="none" width="" height="" />
                                                                        <?php } ?>
                                                                    </div>                     
                                                                </div>
                                                                <div class="col">
                                                                    <p><?php echo $value['biografia']; ?></p>
                                                                    <div class="line-i" style="border-bottom-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);"></div>
                                                                    <div class="line-d"></div>																
                                                                </div>
                                                                <div class="col">
                                                                    <?php if (!empty($value['facebook'])) { ?>
                                                                        <a class="fb" href="http://www.facebook.com/<?php echo $value['facebook']; ?>" target="_blank"></a>
                                                                    <?php } ?>
                                                                    <?php if (!empty($value['instagram'])) { ?>
                                                                        <a class="ig" href="http://www.instagram.com/<?php echo $value['instagram']; ?>" target="_blank"></a>
                                                                    <?php } ?>
                                                                    <?php if (!empty($value['youtube'])) { ?>
                                                                        <a class="yt" href="http://www.youtube.com/<?php echo $value['youtube']; ?>" target="_blank"></a>
                                                                    <?php } ?>
                                                                    <?php if (!empty($value['sitioweb'])) { ?>
                                                                        <a class="web" href="<?php echo $value['sitioweb']; ?>" target="_blank"></a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if (!empty($value['telefono'])) {
                                                                        $wp = '';
                                                                        for ($i = 0; $i < strlen($value['telefono']); $i++) {
                                                                            if (is_numeric($value['telefono'][$i])) {
                                                                                $wp .= $value['telefono'][$i];
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <a class="tel" href="https://wa.me/<?php echo $wp; ?>?text=Hola!, te escribo a través de expohobby.tv!"></a>
                                                                    <?php } ?>

                                                                </div>
                                                            </div> 
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>





                                    </div>

                                    <div class="auspicia">
                                        <h5 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Auspician</h5>
                                        <ul>
                                            <?php
                                            foreach ($auspiciantes as $value) {
                                                $auspSelect = explode(',', $programa['auspiciante']);
                                                if (in_array($value['cod'], $auspSelect)) {
                                                    ?>
                                                    <li>


                                                        <div class="logo">
                                                            <?php if (!empty($value['logo'])) { ?>
                                                                <img src="recursos/auspiciantes/<?php echo $value['logo']; ?>" alt="none" width="" height="" /> 
                                                            <?php } else { ?>
                                                                <img src="recursos/auspiciantes/logo-none.jpg" alt="none" width="" height="" />
                                                            <?php } ?>

                                                        </div>
                                                        <h4 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);"><?php echo $value['nombre']; ?></h4>
                                                        <h3>
                                                            <a data-fancybox data-src="#info-<?php echo $value['cod']; ?>" href="javascript:;" style="background-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">+</a>
                                                        </h3>


                                                        <div id="info-<?php echo $value['cod']; ?>" class="especialista-info" style="display: none; border-top-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">
                                                            <div class="col-i">
                                                                <h2 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);"><?php echo $value['nombre']; ?></h2>
                                                                <div class="sep"></div>
                                                                <?php if (!empty($value['email'])) { ?>
                                                                    <h6 style="color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">Mail: <a href="mailto:<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a></h6>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-d">
                                                                <div class="avatar">
                                                                    <?php if (!empty($value['logo'])) { ?>
                                                                        <img src="recursos/auspiciantes/<?php echo $value['logo']; ?>" alt="none" width="" height="" />
                                                                    <?php } else { ?>
                                                                        <img src="recursos/auspiciantes/logo-none.jpg" alt="none" width="" height="" />
                                                                    <?php } ?>
                                                                </div>                     
                                                            </div>
                                                            <div class="col">
                                                                <p><?php echo $value['biografia']; ?></p>
                                                                <div class="line-i" style="border-bottom-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);"></div>
                                                                <div class="line-d"></div>																
                                                            </div>
                                                            <div class="col">


                                                                <?php if (!empty($value['facebook'])) { ?>
                                                                    <a class="fb" href="http://www.facebook.com/<?php echo $value['facebook']; ?>" target="_blank"></a>
                                                                <?php } ?>
                                                                <?php if (!empty($value['instagram'])) { ?>
                                                                    <a class="ig" href="http://www.instagram.com/<?php echo $value['instagram']; ?>" target="_blank"></a>
                                                                <?php } ?>
                                                                <?php if (!empty($value['youtube'])) { ?>
                                                                    <a class="yt" href="http://www.youtube.com/<?php echo $value['youtube']; ?>" target="_blank"></a>
                                                                <?php } ?>
                                                                <?php if (!empty($value['sitioweb'])) { ?>
                                                                    <a class="web" href="<?php echo $value['sitioweb']; ?>" target="_blank"></a>
                                                                <?php } ?>
                                                                <?php
                                                                if (!empty($value['telefono'])) {
                                                                    $wp = '';
                                                                    for ($i = 0; $i < strlen($value['telefono']); $i++) {
                                                                        if (is_numeric($value['telefono'][$i])) {
                                                                            $wp .= $value['telefono'][$i];
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <a class="tel" href="https://wa.me/<?php echo $wp; ?>?text=Hola!, te escribo a través de expohobby.tv!"></a>
                                                                <?php } ?>

                                                            </div>
                                                        </div>



                                                    </li>

                                                    <?php
                                                }
                                            }
                                            ?>    


                                        </ul>
                                    </div>                                

                                </div>
                            </div>
                            <div class="bloque-ver" style="background-color: rgba(<?php echo $programa['colorR']; ?>,<?php echo $programa['colorG']; ?>,<?php echo $programa['colorB']; ?>,1);">
                                <div class="wrapper">
                                    <?php if ($antPrograma > 0){ ?>
                                        <div class="prev"><a href="ver-programa.php?p=<?php echo ($antPrograma); ?>">Anterior</a></div>
                                    <?php }
                                    if ($nextPrograma > 0){ ?>
                                        <div class="prox"><a href="ver-programa.php?p=<?php echo ($nextPrograma); ?>">Siguiente</a></div>
                                    <?php } ?>
                                </div>	                        
                            </div>

                            <div class="bloque">
                                <div class="wrapper">
                                    <h2 class="violeta-lista">Ver por Técnica</h2>
                                </div>
                                <div class="wrapper">
                                    <div id="tecnicas" class="owl-carousel owl-theme">
                                        <?php
                                        foreach ($tecnicasCarrusel as $temp) {
                                            ?>
                                            <div class="item">
                                                <div>
                                                    <a href="resultado.php?tecnica=<?php echo $temp['cod'] ?>">
                                                        <div class="portada">
                                                            <div class="over-sm">
                                                                <div class="fondo"></div>
                                                                <div class="boton">
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <img src="recursos/tecnica/<?php echo $temp['foto']; ?>" alt="" width="" height="" />
                                                            </div>
                                                    </a>
                                                </div>
                                                <div class="tecnica">
                                                    <h3 class="texto-rosa"><a href="resultado.php?tecnica=<?php echo $temp['cod'] ?>"><?php echo $temp['nombreTec']; ?></a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="bloque">
                            <div class="wrapper">
                                <h2 class="violeta-lista">Ver por Profesional</h2>
                            </div>
                            <div class="wrapper">
                                <div id="prof" class="owl-carousel owl-theme">
                                    <?php
                                    foreach ($especialistaCarrusel as $temp) {
                                        ?>
                                        <div class="item">
                                            <div>
                                                <a href="resultado.php?especialista=<?php echo $temp['cod'] ?>">
                                                    <div class="portada">
                                                        <div class="over-sm">
                                                            <div class="fondo"></div>
                                                            <div class="boton">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <img src="recursos/especialista/<?php echo $temp['foto']; ?>" alt="" width="" height="" />
                                                        </div>
                                                </a>    
                                            </div>
                                            <div class="tecnica">
                                                <h3 class="texto-rosa"><a href="resultado.php?especialista=<?php echo $temp['cod']; ?>"><?php echo $temp['nombreEsp']; ?></a></h3>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php
            include 'footer.php';
            ?> 
        </div>

    </div>

</body>
</html>