<?php

include_once 'includes/connect.php';
error_reporting(0);

class funciones {

    private $db;
    private $mysqli;
    private $lista_temporadas;
    private $lista_especialistas;
    private $lista_programas;
    private $lista_auspiciantes;
    private $lista_tecnicas;
    private $lista_estrenos;
    private $carrusel_estrenos;
    private $carrusel_especialista;
    private $sliderCentral;
    private $lista_publicidad;
    private $resultadosFiltros;
    private $fichaPrograma;
    private $listaTecnicaResultado;

    public function __construct() {
        $this->db = Database::getInstance();
        $this->mysqli = $this->db->getConnection();

//        $this->datosSesion = $_SESSION;
    }

    //**** METODOS PARA TEMPORADAS*****
    private function getTemporadas() {
        $query = "SELECT id,nombre,descripcion,finicio,foto,status FROM temporada t "
                . " WHERE t.status=1 ORDER BY t.finicio desc ";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $finicio, $foto, $status);

            while ($stmt->fetch()) {

                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listTemp[] = array("cod" => $id, "nombre" => $nombre, "descripcion" => $descripcion, "finicio" => $finicio, "foto" => $foto, "status" => $status);
            }
        }
        return $listTemp;
    }

    public function verTemporadas() {
        $this->lista_temporadas = $this->getTemporadas();
        return $this->lista_temporadas;
    }

    //**** METODOS PARA TECNICAS*****

    private function getTecnicas() {
        $query = "SELECT id,nombre,descripcion,foto,status FROM tecnica t "
                . " where t.status=1 ORDER BY t.nombre asc";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $foto, $status);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listTecn[] = array("cod" => $id, "nombre" => $nombre, "descripcion" => $descripcion, "foto" => $foto, "status" => $status);
            }
        }
        return $listTecn;
    }

    public function verTecnicas() {
        $this->lista_tecnicas = $this->getTecnicas();
        return $this->lista_tecnicas;
    }

    //**** METODOS PARA ESPECIALISTAS*****

    private function getEspecialista() {
        $query = "SELECT id,nombre,biografia,foto,telefono,email,facebook,instagram,youtube,sitioweb,status FROM especialista t "
                . " where t.status=1 ORDER BY t.nombre asc";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $biografia, $foto, $telefono, $email, $facebook, $instagram, $youtube, $sitioweb, $status);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listEsp[] = array("cod" => $id, "nombre" => $nombre, "biografia" => $biografia, "foto" => $foto, "telefono" => $telefono, "email" => $email, "facebook" => $facebook, "instagram" => $instagram, "youtube" => $youtube, "sitioweb" => $sitioweb, "status" => $status);
            }
        }
        return $listEsp;
    }

    public function verEspecialistas() {
        $this->lista_especialistas = $this->getEspecialista();
        return $this->lista_especialistas;
    }

    //**** METODOS PARA HOME*****

    private function getEstrenos() {
        $query = "
SELECT * FROM(
(SELECT p.id as id,p.titulo as titulo, te.nombre as nombre, te.finicio as fechaInicio,e.nombre as nombreEsp,t.nombre as nombreTec,p.foto as foto,e.id idEsp 
                , p.fecha_estreno festreno, t.id as tecId FROM programa p
                Inner JOIN especialista e ON p.especialista = e.id
                Inner JOIN tecnica t on p.tecnica = t.id
                Inner JOIN temporada te on p.temporada = te.id
                WHERE p.status = 1 and p.fecha_estreno > date(NOW())
 )
UNION ALL
(SELECT p.id as id,p.titulo as titulo, te.nombre as nombre, te.finicio as fechaInicio,e.nombre as nombreEsp,t.nombre as nombreTec,p.foto as foto,e.id idEsp 
                , p.fecha_estreno festreno, t.id as tecId 
 				FROM capitulosMasters p
                Inner JOIN especialista e ON p.especialista = e.id
                Inner JOIN tecnica t on p.tecnica = t.id
                Inner JOIN temporada te on p.temporada = te.id
                WHERE p.status = 1 and p.fecha_estreno > date(NOW())
)
) as a 
Order by festreno asc
LIMIT 8
";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $nombre, $fechaInicio, $nombreEsp, $nombreTec, $foto, $idEsp, $festreno, $tecId);

            while ($stmt->fetch()) {
                $listEstrenos[] = array("cod" => $id, "titulo" => $titulo, "nombre" => $nombre, "finicio" => $fechaInicio, "nombreEsp" => $nombreEsp, "nombreTec" => $nombreTec,
                    "foto" => $foto, "idEsp" => $idEsp, "festreno" => $festreno, "tecId" => $tecId);
            }
        }
        return $listEstrenos;
    }

    public function verEstrenos() {
        $this->lista_estrenos = $this->getEstrenos();
        return $this->lista_estrenos;
    }

    private function getTecnicasCarrusel() {
        $query = "SELECT p.id as id,p.titulo as titulo,t.nombre as nombreTec,t.foto as foto,t.id idTec 
                FROM programa p
                Inner JOIN tecnica t on p.tecnica = t.id
                WHERE p.status = 1
                GROUP BY t.id";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $nombreTec, $foto, $idTec);

            while ($stmt->fetch()) {
                $listEstrenos[] = array("cod" => $id, "titulo" => $titulo, "nombreTec" => $nombreTec, "foto" => $foto, "idTec" => $idTec);
            }
        }
        return $listEstrenos;
    }

    public function verTecnicasCarrusel() {
        $this->carrusel_estrenos = $this->getTecnicasCarrusel();
        return $this->carrusel_estrenos;
    }

    /*
      private function getEspecialistaCarrusel() {
      $query = "SELECT p.id as id,p.titulo as titulo, te.nombre as nombre, te.finicio as fechaInicio,e.nombre as nombreEsp,t.nombre as nombreTec, e.foto as foto,e.id idEsp FROM programa p
      Inner JOIN especialista e ON p.especialista = e.id
      Inner JOIN tecnica t on p.tecnica = t.id
      Inner JOIN temporada te on p.temporada = te.id
      WHERE p.status = 1
      GROUP BY e.id";

      if ($stmt = $this->mysqli->prepare($query)) {
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($id, $titulo, $nombre, $fechaInicio, $nombreEsp, $nombreTec, $foto, $idEsp);

      while ($stmt->fetch()) {
      $listEspecialista[] = array("cod" => $id, "titulo" => $titulo, "nombre" => $nombre, "finicio" => $fechaInicio, "nombreEsp" => $nombreEsp, "nombreTec" => $nombreTec, "foto" => $foto, "idEsp" => $idEsp);
      }
      }
      return $listEspecialista;
      }

     */

    private function getEspecialistaCarrusel() {

        $query = "SELECT id,nombre,foto FROM especialista t "
                . " WHERE t.status = 1 ORDER BY t.nombre asc";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $foto);

            while ($stmt->fetch()) {
                $listEspecialista[] = array("cod" => $id, "nombreEsp" => $nombre, "foto" => $foto, "idEsp" => $id);
            }
        }
        return $listEspecialista;
    }

    public function verEspecialistaCarrusel() {
        $this->carrusel_especialista = $this->getEspecialistaCarrusel();
        return $this->carrusel_especialista;
    }

    private function getSliderCentral() {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.especialista especialista,p.r rojo, p.g green,p.b blue, p.fecha_estreno fechaE, p.fotoSlider foto, t.id id
                FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                Where p.status=1
                AND p.fecha_estreno <= now()
                ORDER BY p.fecha_estreno desc
                Limit 4";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $texto1, $texto2, $rojo, $green, $blue, $fechaE, $foto, $idtec);

            while ($stmt->fetch()) {
                $sliderhome[] = array("cod" => $id, "titulo" => $titulo, "texto1" => $texto1, "texto2" => $texto2, "rojo" => $rojo, "green" => $green, "blue" => $blue, "fechaE" => $fechaE, "foto" => $foto, "idtec" => $idtec);
            }
        }
        return $sliderhome;
    }

    public function verSliderCentral() {
        $this->sliderCentral = $this->getSliderCentral();
        return $this->sliderCentral;
    }

    private function getProgramas() {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion, p.status FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                ORDER BY te.cdate desc, p.episodio desc
                ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status);
            }
        }
        return $listPro;
    }

    public function verProgramas() {
        $this->lista_programas = $this->getProgramas();
        return $this->lista_programas;
    }

    private function getMasVistos() {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,t.id idtecnica,te.nombre temporada,te.id idTemp,p.foto foto,
                p.episodio episodio,p.especialista especialista, p.tipo tipo,p.tecnica idTecnicas 
                FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                WHERE p.status = 1
                and p.fecha_estreno <= date(NOW())
                ORDER BY p.visitas desc LIMIT 20
                ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $idtecnica, $temporada, $idTemp, $foto, $episodio, $especialista, $tipo, $idTecnicas);

            while ($stmt->fetch()) {

                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "temporada" => $temporada,
                    "especialista" => $especialista, "foto" => $foto, "episodio" => $episodio, "idTec" => $idtecnica, "idTemp" => $idTemp, "tipo" => $tipo, "idTecnicas" => $idTecnicas);
            }
        }
        return $listPro;
    }

    public function verMasVisitados() {
        $this->lista_programas = $this->getMasVistos();
        return $this->lista_programas;
    }

    private function getPublicidad() {
        $query = "SELECT p.id,p.nombre,p.descripcion,p.url_video video,p.tecnica tecnica,p.status FROM `publicidad` p 
                INNER JOIN tecnica t ON p.tecnica = t.id 
                 ORDER BY p.cdate DESC";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $video, $tecnica, $status);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listPubli[] = array("cod" => $id, "nombre" => $nombre, "descripcion" => $descripcion, "video" => $video, "tecnica" => $tecnica, "status" => $status);
            }
        }
        return $listPubli;
    }

    public function verPublicidad() {
        $this->lista_publicidad = $this->getPublicidad();
        return $this->lista_publicidad;
    }

    private function getAuspiciante() {
        $query = "SELECT id,nombre,descripcion,logo,email,facebook,instagram,youtube,web,status"
                . " FROM auspiciante a where a.status=1 "
                . " ORDER BY a.nombre asc";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $logo, $email, $facebook, $instagram, $youtube, $web, $status);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listEsp[] = array("cod" => $id, "nombre" => $nombre, "desc" => $descripcion, "logo" => $logo, "email" => $email, "facebook" => $facebook, "instagram" => $instagram, "youtube" => $youtube, "sitioweb" => $web, "status" => $status);
            }
        }
        return $listEsp;
    }

    public function verAuspiciantes() {
        $this->lista_auspiciantes = $this->getAuspiciante();
        return $this->lista_auspiciantes;
    }

    private function agregarTecnica($nombre, $desc, $img) {

        $query = "INSERT INTO tecnica (nombre,descripcion,foto,cdate,status) VALUE (?,?,?,NOW(),1)";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("sss", $nombre, $desc, $img);
            if ($stmt->execute()) {
                return '1';
            } else {
                return '0';
            }
        }
    }

    public function nuevaTecnica($nombre, $desc, $img) {
        $nuevaTec = $this->agregarTecnica($nombre, $desc, $img);
        return $nuevaTec;
    }

    private function agregarTemporada($nombre, $desc, $finicio, $img) {

        $query = "INSERT INTO temporada (nombre,descripcion,foto,finicio,cdate,status) VALUE (?,?,?,?,NOW(),1)";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ssss", $nombre, $desc, $img, $finicio);
            if ($stmt->execute()) {
                $stmt->close();
                return '1';
            } else {
                printf("Error: %s.\n", $stmt->error);
                return '0';
            }
        }
    }

    public function nuevaTemporada($nombre, $desc, $finicio, $img) {
        $nuevaTemp = $this->agregarTemporada($nombre, $desc, $finicio, $img);
        return $nuevaTemp;
    }

    private function agregarEspecialista($nombre, $bio, $tel, $img, $mail, $fb, $insta, $web, $youtube) {

        $query = "INSERT INTO especialista (nombre, biografia,foto,telefono,email,facebook,instagram,youtube,sitioWeb,cdate,status) VALUE (?,?,?,?,?,?,?,?,?,NOW(),1)";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("sssssssss", $nombre, $bio, $img, $tel, $mail, $fb, $insta, $youtube, $web);
            if ($stmt->execute()) {
                $stmt->close();
                return '1';
            } else {
                return '0';
            }
        }
    }

    public function nuevoEspecialista($nombre, $bio, $tel, $img, $mail, $fb, $insta, $web, $youtube) {
        $nuevaTemp = $this->agregarEspecialista($nombre, $bio, $tel, $img, $mail, $fb, $insta, $web, $youtube);
        return $nuevaTemp;
    }

    private function agregarAuspiciante($nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube) {

        $query = "INSERT INTO auspiciante (nombre,descripcion,logo,email,web,facebook,instagram,youtube,cdate,status) VALUE (?,?,?,?,?,?,?,?,NOW(),1)";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ssssssss", $nombre, $desc, $img, $mail, $web, $fb, $insta, $youtube);
            if ($stmt->execute()) {
                $stmt->close();
                return '1';
            } else {
                return '0';
            }
        }
    }

    public function nuevoAuspiciante($nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube) {
        $nuevaTemp = $this->agregarAuspiciante($nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube);
        return $nuevaTemp;
    }

    private function agregarPrograma($nombre, $desc, $img, $tecnica, $especialista, $auspiciante, $temporada, $estreno, $url_video, $episodio, $pdf) {

        $query = "INSERT INTO programa (titulo,tecnica,especialista,temporada,auspiciante,fecha_estreno,foto,url_video,fichaPdf,episodio,descripcion,cdate,status)"
                . " VALUE (?,?,?,?,?,?,?,?,?,?,?,NOW(),1)";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("sisisssssss", $nombre, $tecnica, $especialista, $temporada, $auspiciante, $estreno, $img, $url_video, $pdf, $episodio, $desc);
            if ($stmt->execute()) {
                $stmt->close();
                return '1';
            } else {
                return '0';
            }
        }
    }

    public function nuevoPrograma($nombre, $desc, $img, $tecnica, $especialista, $auspiciante, $temporada, $estreno, $url_video, $episodio, $pdf) {
        $nuevaTemp = $this->agregarPrograma($nombre, $desc, $img, $tecnica, $especialista, $auspiciante, $temporada, $estreno, $url_video, $episodio, $pdf);
        return $nuevaTemp;
    }

    private function agregarPublicidad($nombre, $desc, $url, $tecnica) {

        $query = "INSERT INTO publicidad (nombre,descripcion,url_video,tecnica,cdate,status) VALUE (?,?,?,?,NOW(),1)";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ssss", $nombre, $desc, $url, $tecnica);
            if ($stmt->execute()) {
                $stmt->close();
                return '1';
            } else {

                return '0';
            }
        }
    }

    public function nuevaPublicidad($nombre, $desc, $url, $tecnica) {
        $nuevaTemp = $this->agregarPublicidad($nombre, $desc, $url, $tecnica);
        return $nuevaTemp;
    }

    //CAMBIOS DE ESTADO
    public function cambiarEstadoTecnica($id) {
        $resp = $this->changeEstadoTecnica($id);
        return $resp;
    }

    private function changeEstadoTecnica($id) {

        $query = "UPDATE tecnica SET status = (SELECT if (status = '1','0','1') as estado)
WHERE id IN (" . $id . ")";

        if ($stmt = $this->mysqli->prepare($query)) {
            if ($stmt->execute()) {
                $estadoActual = array("respuesta" => '1');
            } else {
                $estadoActual = array("respuesta" => '0');
            }
        }
        return $estadoActual;
    }

    public function cambiarEstadoTemporada($id) {
        $resp = $this->changeEstadoTemporada($id);
        return $resp;
    }

    private function changeEstadoTemporada($id) {

        $query = "UPDATE temporada SET status = (SELECT if (status = '1','0','1') as estado)
WHERE id IN (" . $id . ")";

        if ($stmt = $this->mysqli->prepare($query)) {
            if ($stmt->execute()) {
                $estadoActual = array("respuesta" => '1');
            } else {
                $estadoActual = array("respuesta" => '0');
            }
        }
        return $estadoActual;
    }

    public function cambiarEstadoEspecialista($id) {
        $resp = $this->changeEstadoEspecialista($id);
        return $resp;
    }

    private function changeEstadoEspecialista($id) {

        $query = "UPDATE especialista SET status = (SELECT if (status = '1','0','1') as estado)
WHERE id IN (" . $id . ")";

        if ($stmt = $this->mysqli->prepare($query)) {
            if ($stmt->execute()) {
                $estadoActual = array("respuesta" => '1');
            } else {
                $estadoActual = array("respuesta" => '0');
            }
        }
        return $estadoActual;
    }

    public function cambiarEstadoPrograma($id) {
        $resp = $this->changeEstadoPrograma($id);
        return $resp;
    }

    private function changeEstadoPrograma($id) {

        $query = "UPDATE programa SET status = (SELECT if (status = '1','0','1') as estado)
WHERE id IN (" . $id . ")";

        if ($stmt = $this->mysqli->prepare($query)) {
            if ($stmt->execute()) {
                $estadoActual = array("respuesta" => '1');
            } else {
                $estadoActual = array("respuesta" => '0');
            }
        }
        return $estadoActual;
    }

    public function cambiarEstadoAuspiciante($id) {
        $resp = $this->changeEstadoAuspiciante($id);
        return $resp;
    }

    private function changeEstadoAuspiciante($id) {

        $query = "UPDATE auspiciante SET status = (SELECT if (status = '1','0','1') as estado)
WHERE id IN (" . $id . ")";

        if ($stmt = $this->mysqli->prepare($query)) {
            if ($stmt->execute()) {
                $estadoActual = array("respuesta" => '1');
            } else {
                $estadoActual = array("respuesta" => '0');
            }
        }
        return $estadoActual;
    }

    public function cambiarEstadoPublicidad($id) {
        $resp = $this->changeEstadoPublicidad($id);
        return $resp;
    }

    private function changeEstadoPublicidad($id) {

        $query = "UPDATE publicidad SET status = (SELECT if (status = '1','0','1') as estado)
WHERE id IN (" . $id . ")";

        if ($stmt = $this->mysqli->prepare($query)) {
            if ($stmt->execute()) {
                $estadoActual = array("respuesta" => '1');
            } else {
                $estadoActual = array("respuesta" => '0');
            }
        }
        return $estadoActual;
    }

    //MODIFICACIONES UPDATE
    private function getTecnicasXID($id) {
        $query = "SELECT nombre,descripcion,foto FROM tecnica t WHERE t.id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($nombre, $descripcion, $foto);

            if ($stmt->fetch()) {
                $listTecn = array("nombre" => $nombre, "descripcion" => $descripcion, "foto" => $foto);
            }
        }
        return $listTecn;
    }

    public function verTecnicasXID($id) {
        $this->lista_tecnicas = $this->getTecnicasXID($id);
        return $this->lista_tecnicas;
    }

    public function verTecnicasFiltro($id) {
        $this->lista_tecnicas = $this->getTecnicasFiltro($id);
        return $this->lista_tecnicas;
    }

    public function verEspFiltro($id) {
        $this->lista_tecnicas = $this->getEspFiltro($id);
        return $this->lista_tecnicas;
    }

    public function verTecFiltro($tempID, $tecID) {
        $this->listaTecnicaResultado = $this->getTecFiltro($tempID, $tecID);
        return $this->listaTecnicaResultado;
    }

//MODIFICACIONES UPDATE
    private function getTecnicasFiltro($id) {
        $query = "select t.id id, t.nombre nombre
                FROM programa p
                INNER JOIN tecnica t ON t.id = p.tecnica
                INNER JOIN temporada te ON te.id = p.temporada";
        if ($id > 0) {
            $query .= " WHERE te.id = ? ";
        }
        $query .= " GROUP by t.id
                ORDER BY t.nombre asc";


        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre);

            while ($stmt->fetch()) {
                $listTecn[] = array("cod" => $id, "nombre" => $nombre);
            }
        }
        return $listTecn;
    }

    private function getEspFiltro($id) {
        $query = "SELECT t.id id, t.nombre nombre
                FROM programa p
                INNER JOIN especialista t ON t.id IN(p.especialista)
                INNER JOIN temporada te ON te.id = p.temporada";

        if ($id > 0) {
            $query .= " WHERE te.id = ? ";
        }
        $query .= " GROUP by t.id
                ORDER BY t.nombre asc";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre);

            while ($stmt->fetch()) {
                $listTecn[] = array("cod" => $id, "nombre" => $nombre);
            }
        }
        return $listTecn;
    }

    private function getTecFiltro($tempID, $tecID) {
        $query = "SELECT t.id id, t.nombre nombre
                FROM programa p
                INNER JOIN tecnica tc ON tc.id IN(p.tecnica)
                INNER JOIN especialista t ON t.id IN(p.especialista)
                INNER JOIN temporada te ON te.id = p.temporada
                ";
        if ($tecID > 0) {
            $query .= " WHERE te.id=? AND tc.id=? ";
        } else {
            $query .= " WHERE te.id=? ";
        }
        $query .= " GROUP by t.id
                ORDER BY t.nombre asc ";



        if ($stmt = $this->mysqli->prepare($query)) {
            if ($tecID > 0) {
                $stmt->bind_param("ii", $tempID, $tecID);
            } else {
                $stmt->bind_param("i", $tempID);
            }
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre);

            while ($stmt->fetch()) {
                $listTecn[] = array("cod" => $id, "nombre" => $nombre);
            }
        }
        return $listTecn;
    }

    private function getTecFiltro_old($id) {
        $query = "SELECT t.id id, t.nombre nombre
                FROM programa p
                INNER JOIN tecnica tc ON tc.id = p.tecnica
                INNER JOIN especialista t ON t.id IN(p.especialista)
                INNER JOIN temporada te ON te.id = p.temporada
                ";
        if ($id > 0) {
            $query .= " WHERE tc.id = ?";
        }
        $query .= " GROUP by t.id
                ORDER BY t.nombre asc";



        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre);

            while ($stmt->fetch()) {
                $listTecn[] = array("cod" => $id, "nombre" => $nombre);
            }
        }
        return $listTecn;
    }

    private function modificoTecnicaDatos($id, $nombre, $desc, $foto) {

        $query = "UPDATE tecnica SET nombre = ?, descripcion = ?,foto = ? WHERE id = ?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("sssi", $nombre, $desc, $foto, $id);
            if ($stmt->execute()) {
                $resultado = array("respuesta" => '1');
            } else {
                $resultado = array("respuesta" => '0');
            }
        }
        return $resultado;
    }

    public function actualizarTecnica($id, $nombre, $desc, $foto) {
        $datos = $this->modificoTecnicaDatos($id, $nombre, $desc, $foto);
        return $datos;
    }

    private function getAuspicianteXID($id) {
        $query = "SELECT id,nombre,descripcion,logo,email,facebook,instagram,youtube,web FROM auspiciante a WHERE a.id=? ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $logo, $email, $facebook, $instagram, $youtube, $web);

            if ($stmt->fetch()) {
                $listEsp = array("nombre" => $nombre, "desc" => $descripcion, "logo" => $logo, "email" => $email, "facebook" => $facebook, "instagram" => $instagram, "youtube" => $youtube, "sitioweb" => $web);
            }
        }
        return $listEsp;
    }

    public function verAuspicianteXID($id) {
        $this->lista_auspiciantes = $this->getAuspicianteXID($id);
        return $this->lista_auspiciantes;
    }

    private function modificoAuspicianteDatos($id, $nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube) {
        $query = "UPDATE auspiciante SET nombre=?,descripcion=?,logo=?,email=?,web=?,facebook=?,instagram=?,youtube=? WHERE id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ssssssssi", $nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube, $id);
            if ($stmt->execute()) {
                $resultado = array("respuesta" => '1');
            } else {
                $resultado = array("respuesta" => '0');
            }
        }
        return $resultado;
    }

    public function actualizarAuspiciante($id, $nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube) {
        $datos = $this->modificoAuspicianteDatos($id, $nombre, $desc, $img, $mail, $fb, $insta, $web, $youtube);
        return $datos;
    }

    private function getTemporadaXID($id) {
        $query = "SELECT id,nombre,descripcion,finicio,foto FROM temporada t WHERE t.id=? ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $finicio, $foto);
            if ($stmt->fetch()) {
                $listTemp = array("cod" => $id, "nombre" => $nombre, "descripcion" => $descripcion, "finicio" => $finicio, "foto" => $foto);
            }
        }
        return $listTemp;
    }

    public function verTemporadaXID($id) {
        $this->lista_temporadas = $this->getTemporadaXID($id);
        return $this->lista_temporadas;
    }

    private function modificoTemporadaDatos($id, $nombre, $desc, $img, $fechaInicio) {
        $query = "UPDATE temporada SET nombre=?,descripcion=?,foto=?,finicio=? WHERE id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ssssi", $nombre, $desc, $img, $fechaInicio, $id);
            if ($stmt->execute()) {
                $resultado = array("respuesta" => '1');
            } else {
                $resultado = array("respuesta" => '0');
            }
        }
        return $resultado;
    }

    public function actualizarTemporada($id, $nombre, $desc, $img, $fechaInicio) {
        $datos = $this->modificoTemporadaDatos($id, $nombre, $desc, $img, $fechaInicio);
        return $datos;
    }

    private function getPublicidadXID($id) {
        $query = "SELECT p.id,p.nombre,p.descripcion,p.url_video video,p.tecnica tecnica FROM `publicidad` p 
                WHERE p.id=? ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $descripcion, $video, $tecnica);
            if ($stmt->fetch()) {
                $listTemp = array("cod" => $id, "nombre" => $nombre, "descripcion" => $descripcion, "video" => $video, "tecnica" => $tecnica);
            }
        }
        return $listTemp;
    }

    public function verPublicidadXID($id) {
        $this->lista_publicidad = $this->getPublicidadXID($id);
        return $this->lista_publicidad;
    }

    private function modificoPublicidadDatos($id, $nombre, $desc, $video, $tecnica) {
        $query = "UPDATE publicidad SET nombre=?,descripcion=?,url_video=?,tecnica=? WHERE id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ssssi", $nombre, $desc, $video, $tecnica, $id);
            if ($stmt->execute()) {
                $resultado = array("respuesta" => '1');
            } else {
                $resultado = array("respuesta" => '0');
            }
        }
        return $resultado;
    }

    public function actualizarPublicidad($id, $nombre, $desc, $video, $tecnica) {
        $datos = $this->modificoPublicidadDatos($id, $nombre, $desc, $video, $tecnica);
        return $datos;
    }

    private function getProgramaXID_OLD($id) {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,r,g,b, p.status, p.tipo tipo FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo);
            if ($stmt->fetch()) {
                $listPro = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "visitas" => $visitas, "colorR" => $r, "colorG" => $g, "colorB" => $b, "tipo" => $tipo);
            }
        }
        return $listPro;
    }

    private function getProgramaXID($id) {
        $query = "SELECT p.id id,p.titulo titulo,p.tecnica tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,r,g,b, p.status, p.tipo tipo 
, mp.titulo1 t1,mp.titulo2 t2, mp.titulo3 t3, mp.titulo4 t4, mp.titulo5 t5,
                mp.archivo1 a1,mp.archivo2 a2,mp.archivo3 a3,mp.archivo4 a4,mp.archivo5 a5                 
FROM programa p 
                INNER JOIN temporada te ON p.temporada = te.id
                LEFT OUTER JOIN materiales_programa mp ON p.id = mp.idPrograma
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo
                    , $t1, $t2, $t3, $t4, $t5, $a1, $a2, $a3, $a4, $a5);
            if ($stmt->fetch()) {
                $listPro = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "visitas" => $visitas, "colorR" => $r, "colorG" => $g, "colorB" => $b, "tipo" => $tipo
                    , "t1" => $t1, "t2" => $t2, "t3" => $t3, "t4" => $t4, "t5" => $t5, "a1" => $a1, "a2" => $a2, "a3" => $a3, "a4" => $a4, "a5" => $a5);
            }
        }
        return $listPro;
    }

    private function getImgProgramaXID($id, $tipo) {
        $query = "SELECT foto FROM fotos_programa WHERE idPrograma=? and status=1 and tipo=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ii", $id, $tipo);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($foto);
            while ($stmt->fetch()) {
                $listPro[] = array("foto" => $foto);
            }
        }
        return $listPro;
    }

    private function getImgXID($id, $tipo) {
        $query = "SELECT foto FROM fotos_programa WHERE idPrograma=? and status=1 and tipo=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ii", $id, $tipo);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($foto);
            while ($stmt->fetch()) {
                $listPro[] = array("foto" => $foto);
            }
        }
        return $listPro;
    }

    private function getCapProgramaXID($id) { //capitulos especiales
        $query = "SELECT p.id id,p.titulo titulo,p.tecnica tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,r,g,b, p.status, p.tipo tipo 
                FROM programa p 
                INNER JOIN temporada te ON p.temporada = te.id
                WHERE p.id IN (?) group BY p.id";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("s", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo);

            while ($stmt->fetch()) {

                $list[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status);
            }
        }
        return $list;
    }

    public function verProgramaXID($id) {
        $this->lista_programa = $this->getProgramaXID($id);
        return $this->lista_programa;
    }

    private function modificoProgramaDatos($id, $nombre, $desc, $img, $tecnica, $especialista, $auspiciante, $temporada, $estreno, $url_video, $episodio, $pdf) {
        $query = "UPDATE programa SET titulo=?,tecnica=?,auspiciante=?,temporada=?"
                . ",especialista=? ,fecha_estreno=?,foto=?,url_video=? ,fichaPdf=?"
                . ",episodio=?,descripcion=? WHERE id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("sisisssssssi", $nombre, $tecnica, $auspiciante, $temporada, $especialista, $estreno, $img, $url_video, $pdf, $episodio, $desc, $id);
            if ($stmt->execute()) {
                $resultado = array("respuesta" => '1');
            } else {
                $resultado = array("respuesta" => '0');
            }
        }
        return $resultado;
    }

    public function actualizarPrograma($id, $nombre, $desc, $img, $tecnica, $especialista, $auspiciante, $temporada, $estreno, $url_video, $episodio, $pdf) {
        $datos = $this->modificoProgramaDatos($id, $nombre, $desc, $img, $tecnica, $especialista, $auspiciante, $temporada, $estreno, $url_video, $episodio, $pdf);
        return $datos;
    }

    private function getEspecialistaXID($id) {
        $query = "SELECT id,nombre,biografia,foto,telefono,email,facebook,instagram,youtube,sitioWeb"
                . " FROM especialista t WHERE t.id=? ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $biografia, $foto, $telefono, $email, $facebook, $instagram, $youtube, $sitioweb);
            if ($stmt->fetch()) {
                $listEsp = array("cod" => $id, "nombre" => $nombre, "biografia" => $biografia, "foto" => $foto, "telefono" => $telefono, "email" => $email, "facebook" => $facebook, "instagram" => $instagram, "youtube" => $youtube, "sitioweb" => $sitioweb);
            }
        }
        return $listEsp;
    }

    public function verEspecialistaXID($id) {
        $this->lista_publicidad = $this->getEspecialistaXID($id);
        return $this->lista_publicidad;
    }

    private function modificoEspecialistaDatos($id, $nombre, $bio, $tel, $img, $mail, $fb, $insta, $web, $youtube) {
        $query = "UPDATE especialista SET nombre=?,biografia=?,foto=?,telefono=?,email=?,facebook=?,instagram=?,youtube=?,sitioWeb=? WHERE id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("sssssssssi", $nombre, $bio, $img, $tel, $mail, $fb, $insta, $youtube, $web, $id);
            if ($stmt->execute()) {
                $resultado = array("respuesta" => '1');
            } else {
                $resultado = array("respuesta" => '0');
            }
        }
        return $resultado;
    }

    public function actualizarEspecialista($id, $nombre, $bio, $tel, $img, $mail, $fb, $insta, $web, $youtube) {
        $datos = $this->modificoEspecialistaDatos($id, $nombre, $bio, $tel, $img, $mail, $fb, $insta, $web, $youtube);
        return $datos;
    }

    private function getProgramasXTemp($id) {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion, p.status FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                WHERE te.id=?
                ORDER BY te.cdate desc, p.episodio desc
                ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status);
            }
        }
        return $listPro;
    }

    public function buscarProgramaXTemp($id) {
        $this->lista_programas = $this->getProgramasXTemp($id);
        return $this->lista_programas;
    }

    public function buscarProgramaXFiltro($filtros) {
        $this->getResultadosFiltros($filtros);
        return $this->resultadosFiltros;
    }

    private function getResultadosFiltros($filtros) {
        if (is_array($filtros)) {
            $id_tecnica = $filtros['idTec'];
            $id_esp = $filtros['idEsp'];
            $id_temp = $filtros['idTemp'];

            $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion, p.status FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id";

            if (($id_temp > 0) || ($id_esp > 0) || ($id_tecnica > 0)) {
                $query = $query . " WHERE ";
            }
            if ($id_temp > 0) {
                $query = $query . " te.id=" . $id_temp;
            }
            if ($id_tecnica > 0) {
                $query = $query . " te.id=?";
            }

            $query = $query . "ORDER BY te.cdate desc, p.episodio desc
                ";

            if ($stmt = $this->mysqli->prepare($query)) {
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status);

                while ($stmt->fetch()) {
                    if ($status == 1) {
                        $status = "ACTIVO";
                    } else {
                        $status = "INACTIVO";
                    }
                    $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                        "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                        "desc" => $descripcion, "status" => $status);
                }
            }
            return $listPro;
        }
    }

    private function getProgramasActivos() {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion, p.status, t.id tecId,te.id tempId,p.tipo tipo FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                Where p.status=1
                and p.fecha_estreno <= date(NOW())
                ORDER BY te.cdate desc, p.episodio desc
                ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status, $tecId, $tempId, $tipo);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status, "tecId" => $tecId, "tempId" => $tempId, "tipo" => $tipo);
            }
        }
        return $listPro;
    }

    public function verProgramasActivos() {
        $this->lista_programas = $this->getProgramasActivos();
        return $this->lista_programas;
    }

    private function getProgramasActivosFiltro($temp_id, $tec_id, $esp_id, $tipo, $ausp) {

        $query = "SELECT p.id id,p.titulo titulo,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion, p.status, p.tecnica tecId,te.id tempId,p.tipo tipo
                FROM programa p 
                INNER JOIN temporada te ON p.temporada = te.id
                ";

        if (($temp_id >= 0) || ($tec_id >= 0) || ($esp_id >= 0) || ($tipo >= 0) || ($ausp >= 0)) {
            $query = $query . " Where p.status=1 AND te.status=1 AND p.url_video <> '' ";
        }
        if ($temp_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $temp_id . "',te.id)";
        }
        if ($tec_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $tec_id . "',p.tecnica)";
        }

        if ($esp_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $esp_id . "',p.especialista)";
        }
        if ($ausp > 0) {
            $query = $query . " AND FIND_IN_SET('" . $ausp . "',p.auspiciante)";
        }

        $query = $query . " AND p.fecha_estreno <= date(NOW()) ";
        $query = $query . " ORDER BY te.finicio desc, p.episodio desc LIMIT 100";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status, $tecId, $tempId, $tipo);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listPro[] = array("cod" => $id, "titulo" => $titulo, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status, "tecId" => $tecId, "tempId" => $tempId, "tipo" => $tipo);
            }
        }
        return $listPro;
    }

    private function getProgramasActivosFiltroResultado($temp_id, $tec_id, $esp_id, $tipo, $ausp) {


        $query = "SELECT id,titulo,auspiciante,temporada,idTemp, especialista,fechaE,foto,url,fichaPdf,episodio,descripcion
                ,status,tecId,tempId,tipo,nroMV 
                FROM vista_programas";

        if (($temp_id >= 0) || ($tec_id >= 0) || ($esp_id >= 0) || ($tipo >= 0) || ($ausp >= 0)) {
            $query = $query . " Where status=1 AND url <> '' ";
        }
        if ($temp_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $temp_id . "',tempId)";
        }
        if ($tec_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $tec_id . "',tecId)";
        }

        if ($esp_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $esp_id . "',especialista)";
        }
        if ($ausp > 0) {
            $query = $query . " AND FIND_IN_SET('" . $ausp . "',auspiciante)";
        }

        if ($tipo > 0) {
            if ($tipo == 2) {
                $query = $query . " AND tipo IN (2,20)";
            } else {
                if ($tipo == 3) {
                    $query = $query . " AND tipo IN (3,30)";
                } else {
                    $query = $query . " AND tipo = " . $tipo;
                }
            }
        }
        
        $query = $query . " AND fechaE <= date(NOW()) ";
        $query = $query . " ORDER BY temporada DESC, episodio + 0 DESC LIMIT 100";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            //$stmt->bind_result($id, $titulo, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status, $tecId, $tempId, $tipo);
            $stmt->bind_result($id, $titulo, $auspiciante, $temporada, $idTemp, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion
                    , $status, $tecId, $tempId, $tipo, $nroMV);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    //  $status = "ACTIVO";
                } else {
                    //  $status = "INACTIVO";
                }
                $listPro[] = array("cod" => $id, "titulo" => $titulo, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status, "tecId" => $tecId, "tempId" => $tempId, "tipo" => $tipo, "nroMV" => $nroMV);
            }
        }
        return $listPro;
    }

    private function getProgramasActivosFiltro_old($temp_id, $tec_id, $esp_id) {

        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion, p.status, t.id tecId,te.id tempId,p.tipo tipo FROM programa p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                ";

        if (($temp_id >= 0) || ($tec_id >= 0) || ($esp_id >= 0)) {
            $query = $query . " Where p.status=1 ";
        }
        if ($temp_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $temp_id . "',te.id)";
        }
        if ($tec_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $tec_id . "',t.id)";
        }

        if ($esp_id > 0) {
            $query = $query . " AND FIND_IN_SET('" . $esp_id . "',p.especialista)";
        }

        $query = $query . " ORDER BY te.cdate desc, p.episodio desc";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $status, $tecId, $tempId, $tipo);

            while ($stmt->fetch()) {
                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "status" => $status, "tecId" => $tecId, "tempId" => $tempId, "tipo" => $tipo);
            }
        }
        return $listPro;
    }

    public function verProgramasActivosFiltro($temp_id, $tec_id, $esp_id, $tipo, $ausp) {
        // $this->lista_programas = $this->getProgramasActivosFiltro($temp_id, $tec_id, $esp_id, $tipo, $ausp);
        $this->lista_programas = $this->getProgramasActivosFiltroResultado($temp_id, $tec_id, $esp_id, $tipo, $ausp);

        return $this->lista_programas;
    }

    public function verProgramasActivosFiltroResultado($temp_id, $tec_id, $esp_id, $tipo, $ausp) {
        $this->lista_programas = $this->getProgramasActivosFiltroResultado($temp_id, $tec_id, $esp_id, $tipo, $ausp);
        return $this->lista_programas;
    }

    private function getPublicidadActiva() {
        $query = "SELECT id,tecnica,url_video FROM publicidad WHERE status = 1 ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $tecnica, $url_video);
            while ($stmt->fetch()) {
                $listEsp[] = array("cod" => $id, "tecnica" => $tecnica, "url" => $url_video);
            }
        }
        return $listEsp;
    }

    public function verPublicidadActiva() {
        $this->lista_publicidad = $this->getPublicidadActiva();
        return $this->lista_publicidad;
    }

    public function verFichaPrograma($id) {
        $this->fichaPrograma = $this->getProgramaXID($id);
        return $this->fichaPrograma;
    }

    public function verImgPrograma($id, $tipo) {
        $this->fichaPrograma = $this->getImgXID($id, $tipo);
        return $this->fichaPrograma;
    }

    public function verImgMaster($id, $tipo) {
        $this->fichaPrograma = $this->getImgXID($id, $tipo);
        return $this->fichaPrograma;
    }

    public function verImgVivo($id, $tipo) {
        $this->fichaPrograma = $this->getImgXID($id, $tipo);
        return $this->fichaPrograma;
    }

    public function verCapitulosPrograma($id) {
        $this->fichaPrograma = $this->getCapProgramaXID($id);
        return $this->fichaPrograma;
    }

    public function verFichaMaster($id) {
        $master = $this->getMasterXID($id);
        return $master;
    }

    public function verFichaCMaster($id) {
        $master = $this->getCMasterXID($id);
        return $master;
    }

    public function verFichaCVivo($id) {
        $master = $this->getCVivosXID($id);
        return $master;
    }

    public function verCapitulosMaster($id) {
        $master = $this->getCapitulosMaster($id);
        return $master;
    }

    public function verCapitulosVivos($id) {
        $vivos = $this->getCapitulosVivo($id);
        return $vivos;
    }

    public function verIdMaster($id) {
        $master = $this->getIdMaster($id);
        return $master;
    }

    public function verIdVivo($id) {
        $vivo = $this->getIdVivo($id);
        return $vivo;
    }

    public function verIdEspecial($id) {
        $especial = $this->getIdEspecial($id);
        return $especial;
    }

    public function verIdCaps($id) {
        $especial = $this->getIdCaps($id);
        return $especial;
    }

    private function getMasterXID($id) {
        $query = "SELECT p.id id,p.titulo titulo,p.tecnica tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,r,g,b, p.status, p.tipo tipo 
                FROM capitulosMasters p 
                INNER JOIN temporada te ON p.temporada = te.id
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo);
            if ($stmt->fetch()) {
                $listPro = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "visitas" => $visitas, "colorR" => $r, "colorG" => $g, "colorB" => $b, "tipo" => $tipo);
            }
        }
        return $listPro;
    }

    private function getIdMaster($id) {
        $query = "SELECT p.idMaster idMaster 
                FROM capitulosMasters p 
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($idMaster);
            if ($stmt->fetch()) {
                $id_master = array("idMaster" => $idMaster);
            }
        }
        return $id_master;
    }

    public function getDatosMaster($id) {
        $query = "SELECT p.titulo titulo,p.r r,p.g g, p.b b 
                FROM master p 
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($titulo, $r, $g, $b);
            if ($stmt->fetch()) {
                $datosMaster = array("titulo" => $titulo, "colorR" => $r, "colorG" => $g, "colorB" => $b);
            }
        }
        return $datosMaster;
    }

    public function getDatosVivo($id) {
        $query = "SELECT p.titulo titulo,p.r r,p.g g, p.b b,p.NroVivo nroVivo 
                FROM vivo p 
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($titulo, $r, $g, $b, $nroVivo);
            if ($stmt->fetch()) {
                $datosVivo = array("titulo" => $titulo, "colorR" => $r, "colorG" => $g, "colorB" => $b, "nroVivo" => $nroVivo);
            }
        }
        return $datosVivo;
    }

    private function getIdVivo($id) {
        $query = "SELECT p.idVivo idVivo 
                FROM capitulosVivos p 
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($idVivo);
            if ($stmt->fetch()) {
                $id_vivo = array("idVivo" => $idVivo);
            }
        }
        return $id_vivo;
    }

    private function getIdEspecial($id) {
        $query = "SELECT p.idEspecial idEspecial, p.idCapitulo idCapitulo 
                FROM capitulosEspeciales p 
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($idEspecial, $idCapitulo);
            if ($stmt->fetch()) {
                $id_especial = array("idEspecial" => $idEspecial, "idCapitulo" => $idCapitulo);
            }
        }
        return $id_especial;
    }

    private function getIdCaps($id) {
        $query = "SELECT p.idCapitulo idCapitulo 
                FROM capitulosEspeciales p 
                WHERE p.idEspecial=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($idCapitulo);
            while ($stmt->fetch()) {
                $id_especial[] = array("idCapitulo" => $idCapitulo);
            }
        }
        return $id_especial;
    }

    private function getCMasterXID($id) {
        $query = "SELECT p.id id,p.titulo titulo,p.tecnica tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,p.r,p.g,p.b, p.status, p.tipo tipo 
                , mp.titulo1 t1,mp.titulo2 t2, mp.titulo3 t3, mp.titulo4 t4, mp.titulo5 t5,
                mp.archivo1 a1,mp.archivo2 a2,mp.archivo3 a3,mp.archivo4 a4,mp.archivo5 a5,ms.NroMaster as nroMaster
                FROM capitulosMasters p 
                INNER JOIN temporada te ON p.temporada = te.id
                INNER JOIN master ms ON p.idMaster = ms.id
                LEFT OUTER JOIN materiales_master mp ON p.id = mp.idPrograma
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo
                    , $t1, $t2, $t3, $t4, $t5, $a1, $a2, $a3, $a4, $a5, $nroMaster);
            if ($stmt->fetch()) {
                $listPro = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "visitas" => $visitas, "colorR" => $r, "colorG" => $g, "colorB" => $b, "tipo" => $tipo
                    , "t1" => $t1, "t2" => $t2, "t3" => $t3, "t4" => $t4, "t5" => $t5, "a1" => $a1, "a2" => $a2, "a3" => $a3, "a4" => $a4, "a5" => $a5
                    , "nroMaster" => $nroMaster);
            }
        }
        return $listPro;
    }

    private function getCVivosXID($id) {
        $query = "SELECT p.id id,p.titulo titulo,p.tecnica tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,p.r,p.g,p.b, p.status, p.tipo tipo
                , mp.titulo1 t1,mp.titulo2 t2, mp.titulo3 t3, mp.titulo4 t4, mp.titulo5 t5,
                mp.archivo1 a1,mp.archivo2 a2,mp.archivo3 a3,mp.archivo4 a4,mp.archivo5 a5,ms.NroVivo nroVivo
                FROM capitulosVivos p 
                INNER JOIN temporada te ON p.temporada = te.id
                INNER JOIN vivo ms ON p.idVivo = ms.id
                LEFT OUTER JOIN materiales_vivo mp ON p.id = mp.idPrograma
                WHERE p.id=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo
                    , $t1, $t2, $t3, $t4, $t5, $a1, $a2, $a3, $a4, $a5, $nroVivo);
            if ($stmt->fetch()) {
                $listPro = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "visitas" => $visitas, "colorR" => $r, "colorG" => $g, "colorB" => $b, "tipo" => $tipo
                    , "t1" => $t1, "t2" => $t2, "t3" => $t3, "t4" => $t4, "t5" => $t5, "a1" => $a1, "a2" => $a2, "a3" => $a3, "a4" => $a4, "a5" => $a5
                    , "nroVivo" => $nroVivo);
            }
        }
        return $listPro;
    }

    private function getCapitulosMaster($id) {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,t.id idtecnica,te.nombre temporada,te.id idTemp,p.foto foto,
                p.episodio episodio,p.especialista especialista, p.tipo tipo,p.tecnica idTecnicas
                , mp.titulo1 t1,mp.titulo2 t2, mp.titulo3 t3, mp.titulo4 t4, mp.titulo5 t5,
                mp.archivo1 a1,mp.archivo2 a2,mp.archivo3 a3,mp.archivo4 a4,mp.archivo5 a5, m.NroMaster nroMaster
                FROM capitulosMasters p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                inner join master m ON p.idMaster = m.id                 
                LEFT OUTER JOIN materiales_programa mp ON p.id = mp.idPrograma
                WHERE p.idMaster=? and p.status=1";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $idtecnica, $temporada, $idTemp, $foto, $episodio, $especialista, $tipo, $idTecnicas, $t1, $t2, $t3, $t4, $t5, $a1, $a2, $a3, $a4, $a5
                    , $nroMaster);

            while ($stmt->fetch()) {

                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "temporada" => $temporada,
                    "especialista" => $especialista, "foto" => $foto, "episodio" => $episodio, "idTec" => $idtecnica, "idTemp" => $idTemp, "tipo" => $tipo, "idTecnicas" => $idTecnicas
                    , "t1" => $t1, "t2" => $t2, "t3" => $t3, "t4" => $t4, "t5" => $t5, "a1" => $a1, "a2" => $a2, "a3" => $a3, "a4" => $a4, "a5" => $a5, "nroMaster" => $nroMaster);
            }
        }
        return $listPro;
    }

    private function getCapitulosVivo($id) {
        $query = "SELECT p.id id,p.titulo titulo,t.nombre tecnica,t.id idtecnica,te.nombre temporada,te.id idTemp,p.foto foto,
                p.episodio episodio,p.especialista especialista, p.tipo tipo,p.tecnica idTecnicas,v.NroVivo nroVivo  
                FROM capitulosVivos p 
                Inner JOIN tecnica t ON p.tecnica = t.id
                INNER JOIN temporada te ON p.temporada = te.id
                Inner JOIN vivo v ON p.idVivo = v.id
                WHERE p.idVivo=? and p.status=1";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $idtecnica, $temporada, $idTemp, $foto, $episodio, $especialista, $tipo, $idTecnicas, $nroVivo);

            while ($stmt->fetch()) {

                $listPro[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "temporada" => $temporada,
                    "especialista" => $especialista, "foto" => $foto, "episodio" => $episodio, "idTec" => $idtecnica, "idTemp" => $idTemp, "tipo" => $tipo, "idTecnicas" => $idTecnicas
                    , "nroVivo" => $nroVivo);
            }
        }
        return $listPro;
    }

    private function getSeparadores() {
        $query = "SELECT id,nombre,url,tipo,status FROM separador s where s.status=1 "
                . " ORDER BY s.id desc";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $url, $tipo, $status);

            while ($stmt->fetch()) {

                if ($status == 1) {
                    $status = "ACTIVO";
                } else {
                    $status = "INACTIVO";
                }
                if ($tipo == 1) {
                    $tipo = "Inicio";
                } else {
                    $tipo = "Fin";
                }
                $listTemp[] = array("cod" => $id, "nombre" => $nombre, "url" => $url, "tipo" => $tipo, "status" => $status);
            }
        }
        return $listTemp;
    }

    public function verSeparadores() {
        $this->lista_temporadas = $this->getSeparadores();
        return $this->lista_temporadas;
    }

    public function verDatosEstrenos() {
        $estrenos = $this->getDatosEstrenos();
        return $estrenos;
    }

    private function getDatosEstrenos() {
        $query = "SELECT p.id id,p.titulo titulo,p.tecnica tecnica,p.auspiciante auspiciante,
                te.nombre temporada, p.especialista especialista, p.fecha_estreno fechaE, p.foto foto, p.url_video url,
                p.fichaPdf, p.episodio,p.descripcion,visitas,r,g,b, p.status, p.tipo tipo 
                FROM programa p 
                INNER JOIN temporada te ON p.temporada = te.id
                WHERE p.fecha_estreno = '2019-10-27'";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $titulo, $tecnica, $auspiciante, $temporada, $especialista, $fechaE, $foto, $url, $fichaPdf, $episodio, $descripcion, $visitas, $r, $g, $b, $status, $tipo);

            while ($stmt->fetch()) {
                $listTemp[] = array("cod" => $id, "titulo" => $titulo, "tecnica" => $tecnica, "auspiciante" => $auspiciante, "temporada" => $temporada,
                    "especialista" => $especialista, "fechaE" => $fechaE, "foto" => $foto, "url" => $url, "fichaPdf" => $fichaPdf, "episodio" => $episodio,
                    "desc" => $descripcion, "visitas" => $visitas, "colorR" => $r, "colorG" => $g, "colorB" => $b, "tipo" => $tipo);
            }
        }
        return $listTemp;
    }

    public function proximoPrograma($proActual, $tipo) {
        $proxPro = $this->proxPrograma($proActual, $tipo);
        return $proxPro;
    }

    private function proxPrograma($proActual, $tipo) {

        switch ($tipo) {
            case '1':
                $tabla = 'programa';
                break;
            case '2':
                $tabla = 'capitulosMasters';
                break;
            case '3':
                $tabla = 'capitulosVivos';
                break;
        }

        $query = "SELECT id FROM " . $tabla . "
                    WHERE id > ?
                    AND status = 1
                    AND fecha_estreno <= date(NOW())
                    Order By id ASC
                    LIMIT 1";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $proActual);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id);
            if ($stmt->fetch()) {
                $proxId = $id;
            }
            return $proxId;
        }
    }

    public function anteriorPrograma($proActual, $tipo) {
        $antPro = $this->antPrograma($proActual, $tipo);
        return $antPro;
    }

    private function antPrograma($proActual) {
        $query = "SELECT id FROM programa
                    WHERE id < ?
                    AND status = 1
                    AND fecha_estreno <= date(NOW())
                    Order By id DESC
                    LIMIT 1";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $proActual);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id);
            if ($stmt->fetch()) {
                $antId = $id;
            }
            return $antId;
        }
    }

}
